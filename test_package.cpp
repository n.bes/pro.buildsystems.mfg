#include <Poco/Environment.h>
#include <Poco/Format.h>
#include <iostream>


int main(int argc, char** argv) {
    std::string version = Poco::format("%d.%d.%d",
        static_cast<int>(Poco::Environment::libraryVersion() >> 24),
        static_cast<int>((Poco::Environment::libraryVersion() >> 16) & 0xFF),
        static_cast<int>((Poco::Environment::libraryVersion() >> 8) & 0xFF)
    );

    std::cout << "POCO version: " << version << std::endl;                  // 200
    return 0;
}
